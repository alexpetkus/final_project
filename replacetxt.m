function go=replacetxt(needle, haystack, newdata, needle2, thk)
%needle is what you are looking for, haystack is the file you are looking
%in
%newdata is the data you are parsing in
%needle='name=CS_Lam';
%haystack='Truss_test.inp';
%newdata=[8.245e+11 8.245e+11 8.31e+11 .23 .3 .5 5.02e+11 5.02e+11];
offset=3; %offset is the number of lines from needle to the data to replace
myformat='%.3e, %.3e, %.3e, %f, %f, %f, %.3e, %.3e\n  %.3e '; %output format, set for abaqus material properties right now
fileid=fopen(haystack,'r+');
line  = 0;
found = false;
while ~feof(fileid)

    tline = fgetl(fileid);
    line = line + 1;

    if ischar(tline) && ~isempty(strfind(tline, needle))
        found = true;
        go=0;
        break;
        
    end

end

if ~found
    line = NaN; end
for k=1:(offset) %index to offset point
    fgetl(fileid);
end
fseek(fileid,0,'cof'); %placing cursor
fprintf(fileid, myformat, newdata); %printing data
fprintf(fileid, '     \n');

fclose(fileid);
offset2=0; %offset is the number of lines from needle to the data to replace
myformat2='%f'; %output format, set for abaqus material properties right now
fileid=fopen(haystack,'r+');
line  = 0;
found = false;
while ~feof(fileid)

    tline = fgetl(fileid);
    line = line + 1;

    if ischar(tline) && ~isempty(strfind(tline, needle2))
        found = true;
        go=0;
        break;
        
    end

end

if ~found
    line = NaN; end
for k=1:(offset2) %index to offset point
    fgetl(fileid);
end
fseek(fileid, 6,'cof'); %placing cursor
fprintf(fileid, myformat2, thk); %printing data
%fprintf(fileid, ' \n');
fclose(fileid); %close the file




go=1;
end