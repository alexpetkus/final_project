function [mass,U, maxmises, minmises]=csv2str(InputFileName)
density=1630; %density of composite material
% Read the input file
fileID = fopen(InputFileName,'r');
C = textscan (fileID, '%s', 'CollectOutput', 1, 'delimiter', ...
    '','whitespace','','endofline','');
fclose(fileID);
% Initialize A
A=cell(1,1);
% Remove newline characters
A{1}=regexprep(C{1}{1},'\r\n|\n|\r','');
% Convert from cell to char
Rec=char(A);

ind = strfind(Rec,'EVOL'); %find all the times in csv file that volume is called out
vol=zeros(numel(ind),1); %initialize volume matrix
for i=1:numel(ind);
    ind1=ind(i)+120;
    ind2=ind1+11;
    if isempty(str2num(Rec(:,ind1:ind2)));
        vol(i)=10;
    else
    vol(i)=str2num(Rec(:,ind1:ind2));
    end
end
mass=sum(vol)*density; %calculates based on density of material

ind3= strfind(Rec, 'U1');
def1=zeros(numel(ind3,1));
for i=1:numel(ind3);
    ind4=ind3(i)+156;
    ind5=ind4+11;
    %Rec(:,ind4:ind5);
    if isempty(str2num(Rec(:,ind4:ind5)));
        defl(i)=100;
    else
    def1(i)=str2num(Rec(:,ind4:ind5));
end
end
ind6= strfind(Rec, 'U2');
def2=zeros(numel(ind6,1));
for i=1:numel(ind6);
    ind7=ind6(i)+156;
    ind8=ind7+11;
   if isempty(str2num(Rec(:,ind7:ind8)));
       def2(i)=100;
   else
    def2(i)=str2num(Rec(:,ind7:ind8));
end
end
ind9= strfind(Rec, 'U3');
def3=zeros(numel(ind9,1));
for i=1:numel(ind9);
    ind10=ind9(i)+156;
    ind11=ind10+11;
    if isempty(str2num(Rec(:,ind10:ind11)));
        def3(i)=100;
    else
   def3(i)=str2num(Rec(:,ind10:ind11));
end
end
U=[def1.^2+def2.^2+def3.^2].^.5; %takes the three vectors and creates a "total deflection"

ind12 = strfind(Rec,'MISES'); %find all the times in csv file that mises is called out
mises=zeros(numel(ind12,1)); %initialize mises matrix
for i=1:numel(ind12)
    ind13=ind12(i)+141;
    ind14=ind13+12;
    if isempty(str2num(Rec(:,ind13:ind14)));
        mises(i)=0;
    else
   mises(i)=str2num(Rec(:,ind13:ind14));
   %Rec(:,ind13:ind14)
    end
end
maxmises=max(mises);
minmises=min(mises);