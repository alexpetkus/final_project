clear
clc
tic %timer
for s=1:20;
%%Calculate material properties
t=rand(6,1).*8;
t=ceil(t);
ply{1}=num2cell([0 90 45 -45 90 0]); %inputs ply tables
ply{2}=num2cell([0 -60 60 0]);
ply{3}=num2cell([0 10 90 -10 0]);
ply{4}=num2cell([0 0]);
ply{5}=num2cell([0 30 90 -30 0]);
ply{6}=num2cell([0 30 60 -60 -30 0]);
ply{7}=num2cell([10 -10]);
ply{8}=num2cell([5 -5]);
lamnam{1,1}='name=CS_Lam';          %inputs each of the sections names to find in the loops below
lamnam{1,2}='Beam Section, elset=CS';
lamnam{2,1}='name=SS_Lam';
lamnam{2,2}='Beam Section, elset=SS';
lamnam{3,1}='name=DT_Lam';
lamnam{3,2}='Beam Section, elset=DT';
lamnam{4,1}='name=TT_Lam';
lamnam{4,2}='Beam Section, elset=TT';
lamnam{5,1}='name=HT_Lam';
lamnam{5,2}='Beam Section, elset=HT';
lamnam{6,1}='name=DT_Lam';
lamnam{6,2}='Beam Section, elset=DT';



%for k=1:numit

for j=1:6

  r=t(j);
  angles=cell2mat(ply{r}); %creates vector of plies based on ply cell array and k
  [newdata, thk]=modulus(angles); %runs to get material properties and thickness for input of plies
  go=replacetxt(lamnam{j,1}, 'Truss_test.inp', newdata, lamnam{j,2}, thk); %replaces text in the input file for the given ply stack
end
%%%Run Abaqus file

%Runs abaqus
!abaqus analysis job=truss_test input=truss_test output_precision=full cpus=4 
pause(1)
while exist('truss_test.lck'),pause(.01),end %pauses loop while file is locked 
%Creates CSV file
!abaqus odbreport job=truss_test step="Apply seat load" history mode=csv 

InputFileName='truss_test.csv'; 
[mass,U]=csv2str(InputFileName); %Reads csv report and outputs mass and deflection matrix
Mass(s)=mass;
Def(s)=U;
num(s)=s;
end
%%%%%Plotting
subplot(2,1,1)
[hAx,hLine1,hLine2] = plotyy(num,Def,num,Mass);

title('Mass and BB Deflection')
xlabel('Iterations')

ylabel(hAx(1),'Total Deflection (m)') % left y-axis
ylabel(hAx(2),'Total mass (kg)') % right y-axis

csvwrite('temp_mass.csv', Mass);
csvwrite('temp_Def.csv', Def);

toc %timer