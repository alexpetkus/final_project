clear
clc
rng(1,'twister') % for reproducibility 
options = gaoptimset('PopulationSize',50,'PlotFcns',{@gaplotpareto, @gaplotscores, @gaplotspread}, 'TolFun', 1e-4,'Generations',5);

FitnessFunction=@fitness;
numberOfVariables = 6;
lb= [1 1 1 1 1 1];
ub= [8 8 8 8 8 8];
tic
[x,Fval, exitflag, Output, scores] = gamultiobj(FitnessFunction,numberOfVariables,[],[],[],[],lb,ub,options);
%length(x);
%X=1:length(x);
%for i=1:length(x)
%    y(i,:)=fitness(x(i,:));
%end
toc
fprintf('The exitflag was : %d\n', exitflag);
fprintf('The number of generations was : %d\n', Output.generations);
fprintf('The number of function evaluations was : %d\n', Output.funccount);

csvwrite('xvals.csv', x);
csvwrite('Fvals.csv', Fval);
x=csvread('xvals.csv');
%[vonmises, totalmass, deflection]=fork_test(x)